<?php

class InvoiceCustomer {
    public $id = null;

    function __construct($id, $name, $address, $zip, $city, $email = null, $type = 1)
    {
        $this->id = $id;
        $this->name = $name;
        $this->address = $address;
        $this->zip = $zip;
        $this->city = $city;
        $this->email = $email;
        $this->type = $type; // 1: person, 2: company
        $this->sendtype = "email"; // Defaults to email
    }

    function setServiceCode($serviceCode)
    {
        $this->serviceCode = $serviceCode;
    }

    function setSendtype($sendtype)
    {
        $this->sendtype = $sendtype;
    }
}