<?php

class InvoiceRow {


    public $freeText = '';

    /**
     * 
     * @param float price in euros
     * @param string title of the product
     * @param integer quantity
     * 
     */
    function __construct($price, $title, $quantity = 1, $taxPercentage = 24.00)
    {
        $this->price = $price;
        $this->vatPerUnit = round($this->price * $taxPercentage / (100 + $taxPercentage), 2);
        $this->priceWithoutTax = $this->price - $this->vatPerUnit;
        $this->title = $title;
        $this->quantity = $quantity;
        $this->taxPercentage = $taxPercentage;
        $this->totalAmount = $this->price * $this->quantity;
        $this->countAmounts();
    }

    function countAmounts()
    {
        if (isset($this->discount)) {
            $this->totalAmount = round(($this->price * $this->quantity) * (100 - $this->discount) / 100, 2);
        }
        $this->vatAmount = round($this->totalAmount *  $this->taxPercentage / (100 + $this->taxPercentage), 2);
        $this->netAmount = $this->totalAmount - $this->vatAmount;
    }
    
    function setTaxPercentage($taxPercentage)
    {
        $this->taxPercentage = $taxPercentage;
        $this->countAmounts();
    }

    function setFreeText($freeText)
    {
        $this->freeText = $freeText;
    }

    function setServiceCode($serviceCode)
    {
        $this->serviceCode = $serviceCode;
    }

    function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    function setDiscount($discountPercentage)
    {
        $this->discount = $discountPercentage;
        $this->countAmounts();
    }
}