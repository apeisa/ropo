<?php

class Invoice {

    public $customer = null;
    public $totalAmount = 0;
    public $netAmount = 0;
    public $vatAmount = 0;
    public $rows = [];
    public $vats = [];
    public $freeText = '';

    public function __construct(InvoiceCustomer $invoiceCustomer)
    {
        $this->customer = $invoiceCustomer;
    }

    public function addRow(InvoiceRow $invoiceRow)
    {
        $this->rows[] = $invoiceRow;
        $this->totalAmount += $invoiceRow->totalAmount;
        $this->netAmount += $invoiceRow->netAmount;
        $this->vatAmount += $invoiceRow->vatAmount;
        if (! isset($this->vats[$invoiceRow->taxPercentage])) {
            $this->vats[$invoiceRow->taxPercentage]['vatAmount'] = 0;
            $this->vats[$invoiceRow->taxPercentage]['netAmount'] = 0;
            $this->vats[$invoiceRow->taxPercentage]['totalAmount'] = 0;
        } 
        $this->vats[$invoiceRow->taxPercentage]['vatAmount'] += $invoiceRow->vatAmount;
        $this->vats[$invoiceRow->taxPercentage]['netAmount'] += $invoiceRow->netAmount;
        $this->vats[$invoiceRow->taxPercentage]['totalAmount'] += $invoiceRow->totalAmount;
    }

    public function setFreeText($freeText)
    {
        $this->freeText = $freeText;
    }

    public function setAdvancePayment($advancePayment)
    {
        $this->advancePayment = $advancePayment;
    }
}