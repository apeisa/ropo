<?php

class Ropo {

    public function __construct($cid, $apicode, $endpoint = "https://beta.ropo24.fi")
    {
        $this->cid = $cid;
        $this->apicode = $apicode;
        $this->batchid = date(\DateTime::ISO8601, time());
        $this->endpoint = $endpoint;
    }

    public function sendInvoiceBatch(InvoiceBatch $invoiceBatch)
    {
        $json = $this->transformInvoiceBatchToRopoJSON($invoiceBatch);
        $response = $this->post("/rest/jobs", $json);
        return $response;
    }

    public function post($action, $payload) {
        return $this->makeRequest($action, "post", $payload);
    }

    public function makeRequest($action, $method = "get", $payload = "")
    {
        $headers = [];
        $ch = curl_init($this->endpoint . $action);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        if ($method == "post")
        {
            curl_setopt($ch, CURLOPT_POST, true);
            if ($payload) curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Content-Length: ' . strlen($payload);
        }

        if (isset($this->token)) $headers[] = 'Authorization: Bearer ' . $this->token;

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Submit the POST request
        $result = curl_exec($ch);

        if (curl_errno($ch)) { 
            print curl_error($ch); 
            return false;
         }
        
        // Close cURL session handle
        curl_close($ch);
        
        return $result;
    }

    public function auth()
    {
        $this->token = $this->getToken();
    }

    public function getToken()
    {
        // Data in JSON format
        $data = array(
            'cid' => $this->cid,
            'apicode' => $this->apicode
        );
        
        $payload = json_encode($data);

        $result = $this->post('/rest/token', $payload);

        if ($result) {
            $data = json_decode($result);
            return $data->token;
        }
    }

    public function transformInvoiceBatchToRopoJSON(InvoiceBatch $invoiceBatch)
    {
        $stream['datastream']['dataset']    = [];
        $stream['datastream']['batchid']    = $this->batchid;

        // InvoiceBatch groups invoices by customer id
        foreach($invoiceBatch->customers as $invoiceGroup) 
        {
            foreach($invoiceGroup as $invoice) 
            {
                $dataset = [];
                $dataset['jobtype']         = 0; //  0 = lasku, 1=maksumuistutus, 2=Perint�, 300=tulostuspalvelu, 301=postituspalvelu
                
                if ($invoice->customer->type == 1) {
                    $dataset['company']          = $invoice->customer->name;
                } else {
                    $dataset['person']          = $invoice->customer->name;
                }
                $dataset['address']         = $invoice->customer->address;
                $dataset['postcode']        = $invoice->customer->zip;
                $dataset['city']            = $invoice->customer->city;
                $dataset['custnum']         = $invoice->customer->id;
                $dataset['customertype']    = $invoice->customer->type; // 1=company, 2=person
                $dataset['email']           = $invoice->customer->email;
                $dataset['sendtype']        = $invoice->customer->sendtype;
                $dataset['ownref']          = $invoice->customer->id; // We probably need invoiceId here (which finally maps to PW page id for the invoice)
                $dataset['billdate']        = date('Y-m-d');
                $dataset['paydate']         = date("Y-m-d", strtotime('+14 days'));
                $dataset['freetext']        = $invoice->freeText;
                
                if (isset($invoice->customer->serviceCode))
                {
                    $dataset['servicecode']     = $invoice->customer->serviceCode; 
                }
                
                if (isset($invoice->advancePayment))
                {
                    $dataset['advancepayment'] = $invoice->advancePayment;
                }

                foreach($invoice->rows as $invoiceRow)
                {
                    $payrow = [];
                    $payrow['desc']         = $invoiceRow->title;
                    $payrow['count']        = $invoiceRow->quantity;
                    $payrow['amount']       = $invoiceRow->priceWithoutTax;
                    $payrow['taxpr']        = $invoiceRow->taxPercentage;
                    if (isset($invoiceRow->accountId))
                        $payrow['accountid']= $invoiceRow->accountId;
  
                    $payrow['netamount']    = $invoiceRow->netAmount;
                    $payrow['vatamount']    = $invoiceRow->vatAmount;
                    $payrow['totalamount']  = $invoiceRow->totalAmount;
                    $payrow['freetext']     = $invoiceRow->freeText;

                    if (isset($invoiceRow->discount))
                    {
                        $payrow['discount'] = $invoiceRow->discount;
                    }

                    $dataset['payrow'][] = $payrow;
                }
/*  It seems that we don't need to add these, ROPO will calc it for us
                foreach($invoice->vats as $taxpr => $vat) {
                    $taxrow = [];
                    $taxrow['taxpr']        = $taxpr;
                    $taxrow['netamount']    = $vat['netAmount'];
                    $taxrow['vatamount']    = $vat['vatAmount'];
                    $taxrow['totalamount']  = $vat['totalAmount'];
                    $dataset['taxrow'][] = $taxrow;
                }


                $dataset['netamount']    = $invoice->netAmount;
                $dataset['vatamount']    = $invoice->vatAmount;
                $dataset['totalamount']  = $invoice->totalAmount;
*/
                $stream['datastream']['dataset'][] = $dataset;
                
            }
        }

        return json_encode($stream, JSON_PRETTY_PRINT);
    }

}