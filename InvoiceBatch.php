<?php

include(__DIR__ . "/Invoice.php");
include(__DIR__ . "/InvoiceRow.php");
include(__DIR__ . "/InvoiceCustomer.php");

/**
 * Invoice Batch keeps bunch of invoices that can be processed all in once. This makes it easy to manage 
 * billing scenarios where same customer can have multiple reasons for invoice but we want only to send 
 * him one invoice. 
 */

class InvoiceBatch {

    public $totalAmount = 0;
    public $customers = [];

    /**
     * Adds invoice to this invoice batch. Invoice always needs customer 
     * 
     * @param Invoice
     * 
     * @return InvoiceBatch
     */
    public function addInvoice(Invoice $invoice)
    {
        $this->customers[$invoice->customer->id][] = $invoice;
        $this->totalAmount += $invoice->totalAmount;

        return $this;
    }

    /**
     * Adds invoicerow for the current customer. If there is already an invoice for the same customer, 
     * the invoice row will be added to earlier invoice. This makes sure only one invoice per customer.
     * 
     * @param InvoiceRow $invoiceRow
     * @param InvoiceCustomer $invoiceCustomer
     * 
     * @return Invoice $invoice returns the Invoice that the row was added into. 
     */
    public function addInvoiceRowForCustomer(InvoiceRow $invoiceRow, InvoiceCustomer $invoiceCustomer)
    {
        // If there is already a customer, we use his/her invoice
        if ($invoice = $this->getFirstInvoiceByCustomer($invoiceCustomer))
        {
            // All good everything -Cheek
        }
        else 
        {
            $invoice = new Invoice($invoiceCustomer);
            $this->addInvoice($invoice);
        }

        $invoice->addRow($invoiceRow);
        $this->totalAmount += $invoiceRow->totalAmount;
    }

    /**
     * 
     * @param InvoiceCustomer $invoiceCustomer
     *  
     * @return array containing all Invoices by this customer. Empty array in case of 0 invoices.
     */
    public function getInvoicesByCustomer(InvoiceCustomer $invoiceCustomer)
    {
        return (isset($this->customers[$invoiceCustomer->id])) ? $this->customers[$invoiceCustomer->id] : [];
    }

    /**
     * 
     * @param InvoiceCustomer $invoiceCustomer
     * 
     * @return Invoice|null returns first Invoice by customer or null if there is no invoices  
     */
    public function getFirstInvoiceByCustomer(InvoiceCustomer $invoiceCustomer)
    {
        $invoices = $this->getInvoicesByCustomer($invoiceCustomer);
        if (count($invoices)) return $invoices[0];
        else return null;
    }
}