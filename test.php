<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * InvoiceBatchin testaamista ja esimerkkiä. Kyseessä siis neljän luokan kombo, joilla "nippulaskuttaminen" helpottuu.
 * 
 * InvoiceBatch: pitää sisällään kaikki laskut, joita nippuu lisätty. Tätä on suoraviivainen käyttää ilman, että täytyy 
 * jatkuvasti miettiä ollaanko jotain asiakasta lisätty vielä nippuun vai ei.
 * 
 * Invoice: lasku, sisältää aina asiakkaan ja yhden tai useamman laskurivin
 * 
 * InvoiceRow: laskurivi sisältää hinnan, nimen, verotiedot, kappalemäärät, alennukset jne
 * 
 * InvoiceCustomer: asiakkaan tiedot
 */

/**
 * Testiskenaariossa laskutetaan kolmea ihmistä, joista kahdelle tulee useampi laskurivi 
 * (yhdelle kolme, yhdelle kaksi ja yhdelle yksi).
 */

include(__DIR__ . "/InvoiceBatch.php");
include(__DIR__ . "/Ropo.php");



$data = [];
$data[] = [
    'id'            => '111',
    'firstName'     => 'Matti', 
    'lastName'      => 'Virtanen',
    'address'       => 'Kotikuja 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 124,
    'productTitle'  => 'Tuote abc',
    'productCount'  => 1,
];

$data[] = [
    'id'            => '222',
    'firstName'     => 'Maija', 
    'lastName'      => 'Kujala',
    'address'       => 'Sibeliuksenkatu 12 A 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 840.00,
    'productTitle'  => 'Ihmetuotetta',
    'productCount'  => 3,
];

$data[] = [
    'id'            => '333',
    'firstName'     => 'Saku', 
    'lastName'      => 'Koivu',
    'address'       => 'Kotikuja 20',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 24.00,
    'productTitle'  => 'Tuote abc',
    'productCount'  => 1,
];

$data[] = [
    'id'            => '111',
    'firstName'     => 'Matti', 
    'lastName'      => 'Virtanen',
    'address'       => 'Kotikuja 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 100,
    'productTitle'  => 'Tuote abc',
    'productCount'  => 1,
    'discountPercentage' => 10
];

$data[] = [
    'id'            => '222',
    'firstName'     => 'Maija', 
    'lastName'      => 'Kujala',
    'address'       => 'Sibeliuksenkatu 12 A 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 24.00,
    'productTitle'  => 'Tuote abc',
    'productCount'  => 1,
    'taxPercentage' => 10
];

$data[] = [
    'id'            => '111',
    'firstName'     => 'Matti', 
    'lastName'      => 'Virtanen',
    'address'       => 'Kotikuja 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 32.00,
    'productTitle'  => 'Tuote X',
    'productCount'  => 1,
];

$data[] = [
    'id'            => '111',
    'firstName'     => 'Matti', 
    'lastName'      => 'Virtanen',
    'address'       => 'Kotikuja 10',
    'zip'           => '00100',
    'city'          => 'Helsinki',
    'productPrice'  => 24.00,
    'productTitle'  => 'Tuote abc',
    'productCount'  => 2
];

$invoiceBatch = new InvoiceBatch();

foreach($data as $billable) {
    
    $invoiceCustomer = new InvoiceCustomer(
        $billable['id'], 
        $billable['firstName'] . " " . $billable['lastName'], 
        $billable['address'], 
        $billable['zip'], 
        $billable['city']
    );
    
    $invoiceRow = new InvoiceRow(
        $billable['productPrice'], 
        $billable['productTitle'], 
        $billable['productCount']
    );
    if (isset($billable['taxPercentage'])) $invoiceRow->setTaxPercentage($billable['taxPercentage']);
    if (isset($billable['discountPercentage'])) $invoiceRow->setDiscount($billable['discountPercentage']);

    $invoiceBatch->addInvoiceRowForCustomer($invoiceRow, $invoiceCustomer);
}

echo "<pre>";
//print_r($invoiceBatch);



$ropo = new Ropo($cid, $apicode);
echo $ropo->transformInvoiceBatchToRopoJSON($invoiceBatch);
//$ropo->auth();
//$response = $ropo->sendInvoiceBatch($invoiceBatch);
/*
echo "<pre>";
var_dump($response);
